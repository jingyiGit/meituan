<?php

namespace JyMeituan\Meituan;

/**
 * 外卖，订单API
 */
trait Order
{
    /**
     * 未开通授权,,根据流水号查询订单
     * https://developer.meituan.com/docs/api/waimai-order-queryByDaySeq
     *
     * @param array $param
     * @return void
     */
    public function queryByDaySeq($param)
    {
        if (!isset($param['date'])) {
            $param['date'] = date('Ymd', time());
        } else if (strlen($param['date']) == 10) {
            $param['date'] = date('Ymd', $param['date']);
        }
        return $this->request('/waimai/order/queryByDaySeq', ['biz' => json_encode($param)]);
    }
    
    /**
     * 帮商家接单
     * https://developer.meituan.com/docs/api/waimai-order-confirm
     *
     * @param array $param
     * @return void
     */
    public function confirmOrder($param)
    {
        return $this->request('/waimai/order/confirm', ['biz' => json_encode($param)]);
    }
    
    /**
     * 查询收货人真实地址
     * https://developer.meituan.com/docs/api/waimai-ng-order-getRealRecipientAddress
     *
     * @param array $param
     * @return false|mixed
     */
    public function getRealRecipientAddress($param)
    {
        if (!isset($param['queryReasonType'])) {
            $param['queryReasonType'] = 1;
        }
        $res = $this->request('/waimai/ng/order/getRealRecipientAddress', ['biz' => json_encode($param)]);
        if (isset($res['code']) && $res['code'] == 'OP_SUCCESS') {
            return $res['data']['recipient_address'];
        } else {
            return false;
        }
    }
}
