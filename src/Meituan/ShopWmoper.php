<?php

namespace JyMeituan\Meituan;

/**
 * 外卖非接单，店铺API
 */
trait ShopWmoper
{
    /**
     * 获取门店详情信息
     * https://developer.meituan.com/docs/api/wmoper-ng-poi-detail
     *
     * @param int $bid 美团门店ID
     * @return false|mixed
     */
    public function shopGet($bid)
    {
        $param = ['epoiIds' => $bid];
        return $this->request('/wmoper/ng/poi/detail', ['biz' => json_encode($param)]);
    }
    
    /**
     * 更新门店信息
     * https://developer.meituan.com/docs/api/wmoper-ng-poiop-poi-save
     *
     * @param int   $bid 美团门店ID
     * @param array $param
     * @return void
     */
    public function shopSave($bid, $param = [])
    {
        if ($shopInfo = $this->shopGet($bid)) {
            $shopInfo = $shopInfo['data'][0];
        } else {
            return false;
        }
        
        $param['name']           = $param['name'] ?: $shopInfo['name'];
        $param['address']        = $param['address'] ?: $shopInfo['address'];
        $param['longitude']      = $param['longitude'] ?: $shopInfo['longitude'];
        $param['latitude']       = $param['latitude'] ?: $shopInfo['latitude'];
        $param['phone']          = $param['phone'] ?: $shopInfo['phone'];
        $param['shipping_fee']   = $param['shipping_fee'] ?: $shopInfo['shipping_fee'];
        $param['shipping_time']  = $param['shipping_time'] ?: $shopInfo['shipping_time'];
        $param['open_level']     = $param['open_level'] ?: $shopInfo['open_level'];
        $param['is_online']      = $param['is_online'] ?: $shopInfo['is_online'];
        $param['third_tag_name'] = $param['third_tag_name'] ?: '便利店,奶茶';
        
        // 开始请求
        return $this->request('/wmoper/ng/poiop/poi/save', ['biz' => json_encode($param)]);
    }
    
    /**
     * 获取门店品类列表
     * https://developer.meituan.com/docs/api/wmoper-ng-poi-poiTag-list
     *
     * @return false|mixed
     */
    public function shopGetTagList()
    {
        return $this->request('/wmoper/ng/poi/poiTag/list', ['biz' => json_encode([])]);
    }
    
    /**
     * 更新门店营业时间
     * https://developer.meituan.com/docs/api/wmoper-ng-poiop-shippingtime-update
     *
     * @param string $shipping_time 门店营业时间（注意格式，且保证不同时间段之间不存在交集），7:00-9:00,11:30-19:00
     * @return false|mixed
     */
    public function shopUpdateShippingtime($shipping_time)
    {
        $param = ['shipping_time' => $shipping_time];
        return $this->request('/wmoper/ng/poiop/shippingtime/update', ['biz' => json_encode($param)]);
    }
}
