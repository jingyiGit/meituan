<?php

namespace JyMeituan\BasicService;

trait BaseConfig
{
  /**
   * 初始化配置
   *
   * @param array  $config           配置
   * @return mixed
   */
  protected function initConfig($config)
  {
    // 检测必要配置项
    $this->checkConfig($config);
    return $config;
  }
  
  /**
   * 检测必要配置项
   *
   * @param $config
   */
  protected function checkConfig($config)
  {
    // app_key
    if (!isset($config['developerId']) || !$config['developerId']) {
      $this->fail('developerId 不能为空');
      
      // app_secret
    } else if (!isset($config['SignKey']) || strlen(trim($config['SignKey'])) != 16) {
      $this->fail('SignKey为空或长度不对');
    }
  }
}
