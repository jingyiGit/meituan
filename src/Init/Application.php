<?php

namespace JyMeituan\Init;

use JyMeituan\Kernel\Response;
use JyMeituan\Meituan\Meituan;
use JyMeituan\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Meituan
{
  use Response;
  use BaseConfig;
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
